﻿using System;

namespace Exporters
{

    //object model mock up to be modified
    public class mapDataModelObject_Ptr
    {
        public mapDataModelObject_Ptr()
        {
            this.mapName = "";
        }
        public mapDataModelObject_Ptr(string mapName)
        {
            this.mapName = mapName ?? throw new ArgumentNullException(nameof(mapName));
        }

        public String mapName { get; set; }
    }

    public class ExportE142_Impl
    {
        public ExportE142_Impl(mapDataModelObject_Ptr mapObject)
        {
            this.mapObject = mapObject ?? throw new ArgumentNullException(nameof(mapObject));
            this.errorMsg = "";
        }

        public ExportE142_Impl(mapDataModelObject_Ptr mapObject, bool hasMetaData, bool hasBinCodeData)
        {
            this.mapObject = mapObject ?? throw new ArgumentNullException(nameof(mapObject));
            this.hasMetaData = hasMetaData;
            this.hasBinCodeData = hasBinCodeData;
            this.errorMsg = "";
        }

        public void exportMap()
        {

        }

        public mapDataModelObject_Ptr mapObject { get; set; }
        public bool isCompatibleWithFormat(String formatName)
        {
            //todo
            return false;
        }
        public bool hasMetaData { get; set; }
        public bool hasBinCodeData { get; set; }

        public String errorMsg { get; set; }
    }
}
