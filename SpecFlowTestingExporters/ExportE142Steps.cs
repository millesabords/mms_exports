﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using TechTalk.SpecFlow;
//using NUnit.Framework;
//using Exporters;
//
//namespace ExportAMapObjectModelSteps
//{
//    [Binding]
//    public class ExportE142Steps
//    {
//        [BeforeScenario]
//        public void BeforeScenario()
//        {
//            //TODO: implement logic that has to run before executing each scenario
//        }
//
//        [AfterScenario]
//        public void AfterScenario()
//        {
//            //TODO: implement logic that has to run after executing each scenario
//        }
//
//        #region fail_scenario1
//        [Given(@"Given Empty data model")]
//        public void GivenEmptyDataModel()
//        {
//            _context.exporter = new ExportE142_Impl(new mapDataModelObject_Ptr());
//        }
//        [When(@"Export to E(.*) action")]
//        public void WhenExportToEAction(int p0)
//        {
//            //_context.exporter.exportMap();
//        }
//
//       /* [When(@"Export-to-E142 action")]
//        public void WhenExportToE142Action()
//        {
//            //_context.exporter.exportMap();
//        }*/
//        
//        [Then(@"An error message is sent with the following content: ""(.*)""")]
//        public void ThenAnErrorMessageIsSentWithTheFollowingContent(string errorMsg)
//        {
//            //Assert.AreEqual(errorMsg, _context.exporter.errorMsg);
//        }
//        #endregion //fail_scenario1
//
//
//        #region basis_scenario1
//
//        [Given(@"short map in data model")]
//        public void GivenShortMapInDataModel()
//        {
//            //_context.exporter = new ExportE142_Impl();
//        }
//
//        [When(@"no extension metadata asked")]
//        public void AndNoExtensionMetaDataAsked()
//        {
//            //Assert.IsTrue(_context.exporter.hasMetaData);
//        }
//
//        [Then("A XML file is generated showing the map bincodes information")]
//        public void AXMLFileIsGeneratedShowingTheMapBinCodesInformation()
//        {
//           //Assert.AreEqual(errorMsg, _context.exporter.tralala);
//           //todo
//        }
//        #endregion //basi_scenario1
//
//        internal ExportE142Steps(ContextExportE142Steps context)
//        {
//            _context = context;
//        }
//        private readonly ContextExportE142Steps _context;
//    }
//
//    internal class ContextExportE142Steps
//    {
//        public ExportE142_Impl exporter { get; set; }
//    }
//}
