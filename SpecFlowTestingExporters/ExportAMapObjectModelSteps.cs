﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using Exporters;

namespace SpecFlowTestingExporters
{
    [Binding]
    public class ExportAMapObjectModelSteps
    {
        #region fail_scenario1
        [Given(@"Empty data model")]
        public void GivenEmptyDataModel()
        {
            _context.exporter = new ExportE142_Impl(new mapDataModelObject_Ptr());
        }
        
        [When(@"Export to Ehundrefortytwo action")]
        public void WhenEhundrefortytwoAction()
        {
            //ScenarioContext.Current.Pending();
        }
        
        [Then(@"An error message is sent with the following content: ""(.*)""")]
        public void ThenAnErrorMessageIsSentWithTheFollowingContent(string errorMsg)
        {
            Assert.AreEqual(errorMsg, _context.exporter.errorMsg);
        }
        #endregion //fail_scenario1

        internal ExportAMapObjectModelSteps(ContextExportE142Steps context)
        {
            _context = context;
        }
        private readonly ContextExportE142Steps _context;
    }
    internal class ContextExportE142Steps
    {
        public ExportE142_Impl exporter { get; set; }
    }
}
