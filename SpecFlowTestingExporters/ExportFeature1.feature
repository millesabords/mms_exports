﻿Feature: ExportAMapObjectModel
	In oder to print as E142 standard xml format a map
	As an operator
	I want the mapmanager to export the map that was already selected and loaded


@fail_scenario1
Scenario: exporting empty data model
	Given Empty data model
	When Export to Ehundrefortytwo action
	Then An error message is sent with the following content: "Empty content given as data model"

#this scenario may be just a developer test
#@fail_scenario2
#Scenario: Given data model without wafer name
#	When Export-to-E142 action
#	Then An error message is sent with the following content: "data model given does not have any wafer name"
#
#@fail_scenario3
#Scenario: Given data model without bincodes data
#	When Export-to-E142 action
#	Then An error message is sent with the following content: "data model does not contain any bin code data"
#
#	#todo iscompatible
#@basis_scenario1
#Scenario: export one short map to E142 xml without extension
#	Given short map in data model
#	When Export-to-E142 action
#	And no extension metadata asked
#	Then a XML file is generated showing the map bincodes information
#
#@complex_scenario1
#Scenario: Given parametric data model
#	When Export-to-E142 action
#	Then An error message is sent with the following content: "E142 format does not manage parametric data"
